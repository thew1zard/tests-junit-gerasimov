package itis.quiz.spaceships.test;

import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;

import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipFleetManagerJUnitTest {
    static SpaceshipFleetManager center;
    static ArrayList<Spaceship> testList;
    @BeforeAll
    static void setUp(){
        center = new CommandCenter();
        testList = new ArrayList<>();
    }

    @BeforeEach
    void listClear(){
        testList.clear();
    }

    @Test
    @DisplayName("Проверка, что возвращается не пустой список мирных кораблей (getAllCivilianShips) ")
    void getAllCivilianShips_returnedCivilianShips_returnTargetShips(){
        testList.add(new Spaceship("1",0,1,2));
        testList.add(new Spaceship("2",1,1,2));
        testList.add(new Spaceship("3",0,1,2));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

        Assertions.assertEquals(2,result.size());
    }

    @Test
    @DisplayName("Проверка, что возвращается пустой список мирных кораблей (getAllCivilianShips)")
    void getAllCivilianShips_returnedCivilianShip_returnEmptyList(){
        testList.add(new Spaceship("1",1,1,2));
        testList.add(new Spaceship("2",1,1,2));
        testList.add(new Spaceship("3",1,1,2));

        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

        Assertions.assertEquals(0,result.size());

    }

    @Test
    @DisplayName("Проверка, что вовзращается не пустой список кораблей с достаточным местом в грузовом отсеке (getAllShipsWithEnoughCargoSpace)")
    void enoughCargoSpace_returnTargetShips(){
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("2",1,0,2));
        testList.add(new Spaceship("3",0,2,2));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList,1);

        Assertions.assertEquals(2,result.size());
    }

    @Test
    @DisplayName("Проверка, что возвращается пустой список кораблей с достаточным местом в грузовом отсеке (getAllShipsWithEnoughCargoSpace)")
    void enoughCargoSpace_returnEmptyList(){
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("2",0,0,2));
        testList.add(new Spaceship("3",0,2,2));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList,3);

        Assertions.assertEquals(0,result.size());
    }
    @Test
    @DisplayName("Проверка, что корабль по имени возвращается (getShipByName)")
    void byName_returnShip(){
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("2",0,0,2));
        testList.add(new Spaceship("3",0,2,2));

        Spaceship result = center.getShipByName(testList, "1");

        Assertions.assertNotNull(result);
    }

    @Test
    @DisplayName("Проверка, что возвращается Null (getShipByName)")
    void byName_returnNull(){
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("2",0,0,2));
        testList.add(new Spaceship("3",0,2,2));

        Spaceship result = center.getShipByName(testList, "5");

        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Проверка, что возвращается корабль (getMostPowerfullShip)")
    void mostPowerfullShip_returnShip(){
        testList.add(new Spaceship("1",1,2,2));
        testList.add(new Spaceship("2",0,0,2));
        testList.add(new Spaceship("3",0,2,2));

        Spaceship result = center.getMostPowerfulShip(testList);

        Assertions.assertNotNull(result);
    }

    @Test
    @DisplayName("Проверка, что возвращается Null (getMostPowerfullShip)")
    void mostPowerfullShip_returnNull(){
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("2",0,0,2));
        testList.add(new Spaceship("3",0,2,2));

        Spaceship result = center.getMostPowerfulShip(testList);

        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Проверка, что возвращается первый по списку корабль (getMostPowerfullShip)")
    void mostPowerfullShip_returnFirstShip(){
        testList.add(new Spaceship("1",2,2,2));
        testList.add(new Spaceship("2",2,0,2));
        testList.add(new Spaceship("3",0,2,2));

        Spaceship result = center.getMostPowerfulShip(testList);

        Assertions.assertEquals("1",result.getName());
    }



}
