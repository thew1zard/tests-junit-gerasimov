package itis.quiz.spaceships.test;

import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import itis.quiz.spaceships.SpaceshipFleetManager;


import java.util.ArrayList;

public class SpaceshipFleetManagerTest {
    SpaceshipFleetManager center;
    ArrayList<Spaceship> testList = new ArrayList<>();

    private boolean getAllCivilianShips_returnedCivilianShips_returnTargetShips(){
        testList.clear();
        testList.add(new Spaceship("1",0,1,2));
        testList.add(new Spaceship("2",1,1,2));
        testList.add(new Spaceship("3",0,1,2));


        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

        if(result == null || result.size() == 0){
            return false;
        }

        return true;

    }

    private boolean getAllCivilianShips_returnedCivilianShip_returnEmptyList(){
        testList.clear();
        testList.add(new Spaceship("1",1,1,2));
        testList.add(new Spaceship("2",1,1,2));
        testList.add(new Spaceship("3",1,1,2));


        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);
        if(result == null || result.size() != 0){
            return false;
        }
        return true;

    }

    private boolean getAllShipsWithEnoughCargoSpace_returnedShipsWithEnoughCargoSpace_returnTargetShips(){
        testList.clear();
        testList.add(new Spaceship("1",0,1,2));
        testList.add(new Spaceship("1",0,0,2));
        testList.add(new Spaceship("1",0,1,2));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList,1);

        if(result == null || result.size() == 0){
            return false;
        }
        return true;

    }

    private boolean getAllShipsWithEnoughCargoSpace_ifSuitableShipsDoNotExist_returnEmptyList(){
        testList.clear();
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("1",0,0,2));
        testList.add(new Spaceship("1",0,2,2));

        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList,5);

        if(result == null || result.size() != 0){
            return false;
        }
        return true;

    }

    private boolean getShipByName_nameOfMethodSpeaksForItself_returnTargetShip() {
        testList.clear();
        testList.add(new Spaceship("1", 0, 2, 2));
        testList.add(new Spaceship("2", 0, 2, 2));
        testList.add(new Spaceship("3", 0, 2, 2));

        Spaceship result = center.getShipByName(testList, "0");

        if (result == null) {
            return false;
        }
        return true;
    }

    private boolean getShipByName_nameOfMethodSpeaksForItself_returnNull(){
        testList.clear();
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("2",0,2,2));
        testList.add(new Spaceship("3",0,2,2));

        Spaceship result = center.getShipByName(testList, "6");

        if(result != null){
            return false;
        }
        return true;

    }

    private boolean getMostPowerfullShip_shipWithHighestFirepower_returnTargetShip(){
        testList.clear();
        testList.add(new Spaceship("1",4,2,2));
        testList.add(new Spaceship("2",3,2,2));
        testList.add(new Spaceship("3",2,2,2));

        Spaceship result = center.getMostPowerfulShip(testList);

        if(result == null){
            return false;
        }
        return true;

    }
    private boolean getMostPowerfullShip_firstShipWithHighestFirepowerInList_returnTargetShip(){
        testList.clear();
        testList.add(new Spaceship("1",4,2,2));
        testList.add(new Spaceship("2",4,2,2));
        testList.add(new Spaceship("3",2,2,2));

        Spaceship result = center.getMostPowerfulShip(testList);

        if(!result.getName().equals(testList.get(0).getName())){
            return false;
        }
        return true;

    }
    private boolean getMostPowerfullShip_suitableShipDoesNotExists_returnNull(){
        testList.clear();
        testList.add(new Spaceship("1",0,2,2));
        testList.add(new Spaceship("2",0,2,2));
        testList.add(new Spaceship("3",0,2,2));

        Spaceship result = center.getMostPowerfulShip(testList);

        if(result != null){
            return false;
        }
        return true;

    }




    public static void main(String[] args) {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        double allGoodCounter = 0;

        if(spaceshipFleetManagerTest.getAllCivilianShips_returnedCivilianShips_returnTargetShips()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else{
            System.out.println("Test 1 is failed!");
        }

        if(spaceshipFleetManagerTest.getAllCivilianShips_returnedCivilianShip_returnEmptyList()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else{
            System.out.println("Test 2 is failed!");
        }

        if(spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_returnedShipsWithEnoughCargoSpace_returnTargetShips()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else {
            System.out.println("Test 3 is failed!");
        }

        if(spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_ifSuitableShipsDoNotExist_returnEmptyList()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else {
            System.out.println("Test 4 is failed!");
        }

        if(spaceshipFleetManagerTest.getShipByName_nameOfMethodSpeaksForItself_returnTargetShip()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else {
            System.out.println("Test 5 is failed!");
        }

        if(spaceshipFleetManagerTest.getShipByName_nameOfMethodSpeaksForItself_returnNull()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else {
            System.out.println("Test 6 is failed!");
        }

        if(spaceshipFleetManagerTest.getMostPowerfullShip_shipWithHighestFirepower_returnTargetShip()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else {
            System.out.println("Test 7 is failed!");
        }

        if(spaceshipFleetManagerTest.getMostPowerfullShip_firstShipWithHighestFirepowerInList_returnTargetShip()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else {
            System.out.println("Test 8 is failed!");
        }

        if(spaceshipFleetManagerTest.getMostPowerfullShip_suitableShipDoesNotExists_returnNull()){
            System.out.println("All good!");
            allGoodCounter += 0.45;
        } else {
            System.out.println("Test 9 is failed!");
        }

        double taskResult = allGoodCounter;

        if(taskResult > 4){
            taskResult = 4;
        }

        System.out.println("Итоговый балл за задание - " + taskResult);

    }


    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }
}
