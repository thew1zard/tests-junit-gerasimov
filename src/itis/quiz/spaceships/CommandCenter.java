package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships){
        int theBiggestFirePower = 0;
        Spaceship shipWithTheBiggestFirePower = null;
        for (Spaceship ship: ships) {
            if(ship.getFirePower() > 0 && ship.getFirePower()>theBiggestFirePower){
                theBiggestFirePower= ship.getFirePower();
                shipWithTheBiggestFirePower = ship;
            }
        }
        return shipWithTheBiggestFirePower;
    }

    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name){
        for (Spaceship ship : ships) {
            if(ship.getName().equals(name)){
                return ship;
            }
        }
        return null;
    }

    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships,Integer cargoSize){
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        for (Spaceship ship: ships) {
            if(ship.getCargoSpace() >= cargoSize){
                shipsWithEnoughCargoSpace.add(ship);
            }
        }
        return shipsWithEnoughCargoSpace;
    }

    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships){
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        for (Spaceship ship : ships) {
            if(ship.getFirePower() == 0){
                civilianShips.add(ship);
            }
        }
        return civilianShips;
    }
}
